import json

def readcfg() -> dict:
        with open('config.json', 'r') as cfg_file:
            f = json.load(cfg_file)
        r = {
            'discord_key' : f['discord_key'],
            'gitlab_api_private_token' : f['gitlab_api_private_token'],
            'sudo_password' : f['sudo']['password']
        }
        return r

cfg = readcfg()
print(cfg['sudo_password'])