from invoke import Task, Collection, Config, Context, Responder, Result
from invoke.config import Config, merge_dicts
import re, sys, os, json
from main import config
# from fabric import Connection

cfg = config.readcfg()

class test_config(Config):
    @staticmethod
    def global_defaults():
        their_defaults = Config.global_defaults()
        my_defaults = {
            'sudo': {
                'password' : cfg['sudo_password']
            }
        }
        return merge_dicts(their_defaults, my_defaults)

@Task
def install_php(c):
    c.sudo('yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm -y')
    c.sudo('yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y')
    c.sudo('yum install yum-utils -y')
    c.sudo('yum-config-manager --enable remi-php73')
    c.sudo('yum install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo -y')

@Task
def install_composer(c):
    c.run()

@Task
def check(c):
    res = dict()
    #php
    try:
        c.run('command -v php')
        res['php'] = True
    except:
        install_php(c)
        try:
            c.run('command -v php')
            res['php'] = True
        except:
            res['php'] = False
    finally:
        print(res['php'])

@Task
def update(c, project, env):
    proj = {
        'dmap' : {
            'dir' : '/home/thaipdt/domains/dmap.vn/public_html/main/dmap_attachted',
            'git' : 'https://' + cfg['git_username'] + ':' + cfg['git_password'] + '@gitlab.com/de_zeddust7/dmap_attachted.git'
        },
        'fmap' : {
            'dir' : '/home/thaipdt/domains/fmap.vn/public_html',
            'git' : 'https://' + cfg['git_username'] + ':' + cfg['git_password'] + '@gitlab.com/de_zeddust7/fmap-.git'
        }
    }
    c.run('cd ' + proj[project]['dir'])
    res = c.run('git pull')
    
