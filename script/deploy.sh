#!/usr/bin/env bash

#--deploy. NOTE: var $2 = branch / $1 = repo
#example ./deploy.sh dmap master
if [ "$PHP_READY" == 1 ] && [ "$GIT_READY" == 1 ]; then
    if [ "$2" ]; then
        if [ "$1" ]; then
            case "$1" in
                dmap)
                    echo "dmap"
                    cd $DMAP_DIR
                    git clone -b $2 https://stiendat:Datdat123@gitlab.com/de_zeddust7/dmap_attachted.git
                    composer install
                    cp -a ./dmap_attachted/public/. ./
                    
                ;;
                fmap)
                    echo "fmap"
                    cd $FMAP_DIR
                    git clone -b $2 https://stiendat:Datdat123@gitlab.com/de_zeddust7/fmap-.git
                    composer install
                ;;
                comteck)
                    echo "comteck"
                    ERR="DMAP REPO NOT FOUND"
                    export ERR
                *)
                    echo "unknow"
                    ERR="PROJECT NOT FOUND"
                ;;
            esac
        fi
    else
        ERR="BRANCH NOT DEFINED"
        export ERR
    fi
else
    ERR="ENV ERROR"
    export ERR
fi
