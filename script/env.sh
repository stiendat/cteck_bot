#!/usr/bin/env bash

#------------------------------------------
#--Install PHP
if [ `command -v php` ]; then
    export PHP_READY=1
elif [ `command -v apt-get` ]; then
    apt-get install php libapache2-mod-php php-mysql -y
    if [ `command -v php` ]; then
        export PHP_READY=1
    else
        export PHP_READY=0
    fi
elif [ `command -v yum` ]; then
    yum install php php-mysql -y
    if [ `command -v php` ]; then
        export PHP_READY=1
    else
        export PHP_READY=0
    fi
fi

#------------------------------------------
#--Check git installation
if [ `command -v git` ]; then
    export GIT_READY=1
elif [ `command -v apt` ]; then
    apt-get install git -y
    if [ `command -v git` ]; then
        export GIT_READY=1
    else
        export GIT_READY=0
    fi
elif [ `command -v yum` ]; then
    yum install git -y
    if [ `command -v git` ]; then
        export GIT_READY=1
    else
        export GIT_READY=0
    fi
fi

#------------------------------------------
#--Check working dir
if [ "$1" ]; then
    DIR=$1
    echo "DIR BIND $1"
    cd $DIR
else
    DIR="ERR"
    echo "DIR NOT FOUND"
fi

#------------------------------------------
#--Install composer
if [ "$DIR" != "ERR" ]; then
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    php composer-setup.php --install-dir=$DIR --filename=composer
    rm -f composer-setup.php
    if [ `command -v composer` ]; then
        echo "composer installed"
    else
        export ERR="COMPOSER BAD INSTALL"
    fi
else
    export ERR="DIR NOT FOUND"
fi

#--Install MariaDB
if [ `command -v mysql` ]; then
    export SQL_READY=1
else
    export SQL_READY=0   
fi