import requests
import bs4
import pprint
import os
import shelve
import json
import time

# ---------------------------------------
class database:
    def __init__(self):
        if os.path.isfile(os.getcwd + '\\db.dat'):
            pass
        else:
            data = shelve.open('db')
            data.close()
        return super().__init__()

    def writedb(self, db, contentHeader, content):
        data = shelve.open(db)
        data[contentHeader] = content
        data.close()
    
    def readdb(self, db, contentHeader) -> dict:
        data = shelve.open(db)
        r = data[contentHeader]
        data.close()
        return r
# ---------------------------------------
class config:
    def __init__(self):
        if os.path.isfile(str(os.getcwd()) + '/config.json'):
            pass
        else:
            f = open('config.json', 'w')
            config_example = {
                'discord_key' : '',
                'gitlab_api_private_token' : '',
                'sudo' : {
                    'password' : ''
                },
                'git' : {
                    'username' : '',
                    'password' : ''
                }
            }
            json.dump(config_example, f)
        return super().__init__()
    
    def readcfg(self) -> dict:
        with open('config.json', 'r') as cfg_file:
            f = json.load(cfg_file)
        r = {
            'discord_key' : f['discord_key'],
            'gitlab_api_private_token' : f['gitlab_api_private_token'],
            'sudo_password' : f['sudo']['password'],
            'git_username' : f['git']['username'],
            'git_password' : f['git']['password']
        }
        return r
# ---------------------------------------
class requests:
    def __init__(self, project_id):
        cfg = config().readcfg()
        self.gitlab_token = cfg.gitlab_api_private_token
        self.url_base = 'https://gitlab.com/api/v4/projects' + project_id
        return super().__init__()

    def get(self, url : str, params = None, header = None, content = None) -> dict:
        r = requests.get(url = self.url_base + url, params = params, headers = header)
        r.raise_for_status()
        res = r.json()
        return res

    def post(self, url : str, params = None, header = None, content = None):
        r = requests.post(url = self.url_base + url, params = params, headers = header, data= content)
        r.raise_for_status()
        return r
# ---------------------------------------
class noti:
    def __init__(self, *args, **kwargs):
        return super().__init__(*args, **kwargs)

cfg = config().readcfg()
print(type(cfg['git_username']))